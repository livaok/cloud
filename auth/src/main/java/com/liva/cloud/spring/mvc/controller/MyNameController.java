package com.liva.cloud.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyNameController {

    @RequestMapping (value = "/name")
    public String getName(){
        return "name";
    }
}