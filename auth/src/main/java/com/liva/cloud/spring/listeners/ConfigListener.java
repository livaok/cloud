package com.liva.cloud.spring.listeners;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ConfigListener implements ApplicationListener<ContextRefreshedEvent> {

    @Value("${cloud2.config.max}")
    private int maxValue;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println("max = " + maxValue);
    }
}