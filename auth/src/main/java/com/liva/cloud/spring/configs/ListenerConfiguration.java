package com.liva.cloud.spring.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.liva.cloud.spring.listeners")
public class ListenerConfiguration {
}
