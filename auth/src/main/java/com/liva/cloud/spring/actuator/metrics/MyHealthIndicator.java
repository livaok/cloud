package com.liva.cloud.spring.actuator.metrics;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MyHealthIndicator implements HealthIndicator {
    @Override
    public Health health() {
        Map<String, Integer> det = new HashMap<>();
        det.put("key", 100);
        det.put("value", 300);
        return Health.up().withDetails(det).build();
    }


}
