package com.liva.cloud.spring.mvc.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/ping")
public class PingRestController {

    @RequestMapping(value = "/{ping}", method = RequestMethod.GET)
    public String getPing(@PathVariable String ping) {
        return ping;
    }
}