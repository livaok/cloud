package com.liva.cloud.spring.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.liva.cloud.spring.actuator.metrics")
public class ActuatorConfiguration {

}
