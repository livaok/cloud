package com.liva.cloud.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/map")
public class IndexController {

    @RequestMapping
    public String index() {
        return "index";
    }
}
